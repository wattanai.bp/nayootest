import React, { useState } from "react";

export default function HeaderPage({ onEnterSearch = () => {} }) {
  const [searchText, setsearchText] = useState("");
  const [selectedType, setselectedType] = useState(null);
  const [TypeMovie, setTypeMovie] = useState([
    {
      name: "หนังล่าสุด",
      img: "/typeLastest.png",
      select: false,
    },
    {
      name: "แอคชั่น",
      img: "/typeAction.png",
      select: false,
    },
    {
      name: "ซีรี่ส์",
      img: "/typeSeries.png",
      select: false,
    },
    {
      name: "การ์ตูน",
      img: "/typeKid.png",
      select: false,
    },
    {
      name: "อนิเมะ",
      img: "/typeLastest.png",
      select: false,
    },
    {
      name: "หนังไทย",
      img: "/typeThai.png",
      select: false,
    },
    {
      name: "หนังฝรั่ง",
      img: "/typeForeign.png",
      select: false,
    },
  ]);
  return (
    <div
      className="mainPaddingHorizontal"
      style={{
        paddingTop: 25,
        paddingBottom: 13,
        borderBottom: "1px solid var(--gray)",
      }}
    >
      <div className="displayFlexRowBetween">
        <div className="displayFlexRowAlignCenter">
          <img
            src="/LOGO.png"
            style={{ width: 123, objectFit: "contain", marginRight: 20 }}
          />
          <div
            className="displayFlexRowAlignCenter"
            style={{ position: "relative" }}
          >
            <img
              src="/search.png"
              style={{
                width: 40,
                objectFit: "contain",
                position: "absolute",
                left: 5,
              }}
            />
            <input
              value={searchText}
              onChange={(e) => setsearchText(e.target.value)}
              onKeyDown={(e) => {
                if (e.key.toLocaleLowerCase() === "enter") {
                  onEnterSearch();
                }
              }}
              placeholder="ค้นหา"
              className="customSearch"
            />
          </div>
        </div>
        <div className="displayFlexRowAlignCenter">
          <div
            className="button"
            style={{
              backgroundColor: "var(--gray2)",
              marginRight: 15,
              border: "1px solid var(--gray2)",
            }}
          >
            Register
          </div>
          <div className="button" style={{ border: "1px solid black" }}>
            Sign In
          </div>
        </div>
      </div>
      <div className="displayFlexRow" style={{marginTop:15}}>
        {TypeMovie.map((item, i) => {
          return (
            <div key={i} className="button" style={{padding:0,marginRight:i!=TypeMovie.length-1?38:0}}>
              {item.name}
            </div>
          );
        })}
      </div>
    </div>
  );
}
