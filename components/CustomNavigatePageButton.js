import React, { useState } from "react";

export default function CustomNavigatePageButton({
  AllPage = 1,
  onChange = () => {},
}) {
  const [currentPageProduct, setcurrentPageProduct] = useState(1);
  return (
    <div
      className="displayFlexRowAlignCenter"
      style={{ marginTop: 20, marginBottom: 20 }}
    >
      <div
        style={{
          marginRight: 5,
          backgroundColor: currentPageProduct <= 1 ? "var(--gray)" : "black",
        }}
        className="buttonNavigatePage"
        onClick={() => {
          if (currentPageProduct > 1) {
            setcurrentPageProduct(currentPageProduct - 1);
            onChange(currentPageProduct - 1);
          }
        }}
      >
        Prev
      </div>
      {Array(AllPage)
        .fill()
        .map((item, i) => {
          return (
            <div
              onClick={()=>{
            setcurrentPageProduct(i + 1);
            onChange(i + 1);
              }}
              style={{ marginRight: 5 }}
              className={
                currentPageProduct === i + 1
                  ? "currentNumberbuttonNavigatePage"
                  : "numberbuttonNavigatePage"
              }
              key={i}
            >
              {i + 1}
            </div>
          );
        })}
      <div
        style={{
          marginRight: 5,
          backgroundColor:
            currentPageProduct >= AllPage ? "var(--gray)" : "black",
        }}
        className="buttonNavigatePage"
        onClick={() => {
          if (currentPageProduct < AllPage) {
            setcurrentPageProduct(currentPageProduct + 1);
            onChange(currentPageProduct + 1);
          }
        }}
      >
        Next
      </div>
    </div>
  );
}
