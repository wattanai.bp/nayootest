import React, { useState } from "react";

export default function BottomAds() {
  const [showAds, setshowAds] = useState(true);
  return showAds ? (
    <div style={{ position: "fixed", bottom: 0, margin: "0px 250px",zIndex:100 }}>
      <div style={{ position: "relative", width: "100%" }}>
        <div
          className="button displayFlexRowAlignCenter"
          style={{
            position: "absolute",
            right: 10,
            top:10,
            padding: "2px 10px",
            backgroundColor: "var(--red)",
          }}
          onClick={() => {
            setshowAds(false);
          }}
        >
          <div style={{ color: "white" }}>ปิด</div>
          <img
            src="/off_close.png"
            style={{
              width: 40,
              objectFit: "contain",
            }}
          />
        </div>
        <img src="/wAds.png" style={{ width: "100%", objectFit: "contain" }} />
      </div>
    </div>
  ) : null;
}
