import React, { useState } from "react";

export default function LeftAds() {
  const [showAds, setshowAds] = useState(true);
  return showAds ? (
    <div style={{ position: "fixed", left: 0, top: 200,zIndex:100 }}>
      <div style={{ position: "relative", width: 234 }}>
        <img
          className="button"
          onClick={() => {
            setshowAds(false);
          }}
          src="/close.png"
          style={{
            width: 45,
            objectFit: "contain",
            position: "absolute",
            right: 0,
            padding:0
          }}
        />
        <img src="/hAds.png" style={{ width: "100%", objectFit: "contain" }} />
      </div>
    </div>
  ) : null;
}
