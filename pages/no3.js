import React, { useEffect, useState } from "react";

export default function No3() {
  const [shop, setshop] = useState([
    {
      price: 25,
      more: 10,
      discount: 20,
      discountType: "percent",
      whenbuyFree: 0,
      free: 0,
    },
    {
      price: 30,
      more: 0,
      discount: 0,
      discountType: null,
      whenbuyFree: 3,
      free: 1,
    },
  ]);
  const [money, setmoney] = useState("");
  const [summaryShop1, setsummaryShop1] = useState(0);
  const [summaryShop2, setsummaryShop2] = useState(0);
  const [amountShop1, setamountShop1] = useState(0);
  const [amountShop2, setamountShop2] = useState(0);
  useEffect(() => {
    calculate();
  }, [money]);

  const calculate = () => {
    for (let index = 0; index < 2; index++) {
      let type = index;
      let sum = "";
      let tempMoney = parseInt(money);
      const calculateValue = shop[type];
      let no = 0;
      if (calculateValue.more > 0) {
        sum = tempMoney / calculateValue.price;
        let discountPrice =
          calculateValue.price -
          calculateValue.price * (calculateValue.discount / 100);
        if (tempMoney / discountPrice >= calculateValue.more) {
          for (let i = 0; tempMoney >= discountPrice; i++) {
            tempMoney -= discountPrice;
            no++;
          }
        } else {
          for (let i = 0; tempMoney >= calculateValue.price; i++) {
            tempMoney -= calculateValue.price;
            no++;
          }
        }
      } else if (calculateValue.whenbuyFree > 0 && calculateValue.free) {
        let tempFree = 0;
        for (let i = 0; tempMoney >= calculateValue.price; i++) {
          tempMoney -= calculateValue.price;
          no++;
          if ((i + 1) % calculateValue.whenbuyFree === 0) {
            tempFree++;
          }
        }
        no += tempFree;
      }
      sum =
        "ซื้อร้านที่" + (type + 1) + " ได้ " + no + " เหลือเงิน " + tempMoney;
      if (type == 0) {
        setamountShop1(no);
        setsummaryShop1(tempMoney);
      } else if (type == 1) {
        setamountShop2(no);
        setsummaryShop2(tempMoney);
      }
    }

    // return sum;
  };
  return (
    <div style={{ padding: 10 }}>
      <div style={{ fontSize: 50 }}>โปรแกรมคำนวณการซื้อของที่ร้านค้า</div>
      <div style={{ padding: "20px 0px" }}>
        <div style={{ fontSize: 30 }}>
          ร้านที่ 1 ราคาสินค้าชิ้นละ 25 บาท เมื่อซื้อ 10 ชิ้นขึ้นไปลดให้ 20%
        </div>
        <div style={{ fontSize: 30 }}>
          ร้านที่ 2 ราคาสินค้าชิ้นละ 30 บาท เมื่อซื้อสินค้า 3 ชิ้น แถมให้ 1 ชิ้น
        </div>
      </div>
      <input
        className="customSearch"
        value={money}
        style={{ paddingLeft: 20 }}
        placeholder={"กรุณากรอกจำนวนเงิน"}
        onChange={(e) => {
          let newvalue = e.target.value;
          if (!isNaN(newvalue)) {
            setmoney(newvalue === "" ? "" : parseFloat(newvalue));
          }
        }}
      />
      {money != "" ? (
        <div>
          <div style={{ fontSize: 30 }}>
            {"ซื้อร้านที่ 1 ได้ " + amountShop1 + " เหลือเงิน " + summaryShop1}
          </div>
          <div style={{ fontSize: 30 }}>
            {"ซื้อร้านที่ 2 ได้ " + amountShop2 + " เหลือเงิน " + summaryShop2}
          </div>
          <div style={{ fontSize: 30, color: "var(--blue2)" }}>
            {amountShop1 === amountShop2 && summaryShop1 === summaryShop2
              ? "ไม่มีร้านที่คุ้มกว่า"
              : "แนะนำให้ซื้อร้านที่ " +
                (amountShop1 > amountShop2
                  ? 1
                  : amountShop1 == amountShop2
                  ? summaryShop1 > summaryShop2
                    ? 1
                    : 2
                  : 2) +
                " จะคุ้มที่สุด"}
          </div>
        </div>
      ) : null}
    </div>
  );
}
