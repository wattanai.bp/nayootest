import React, { useState } from "react";

export default function no4() {
  const [money, setmoney] = useState("");
  const [ProductPrice, setProductPrice] = useState("");
  const calculate = () => {
    console.log('money',money)
    console.log('ProductPrice',ProductPrice)

    let tempMoney = parseInt(money)-parseInt(ProductPrice);
    let sum = "";
    let Fivehundred = 0;
    let Onehundred = 0;
    let Fifty = 0;
    let ten = 0;
    let five = 0;
    let one = 0;
    console.log('tempMoney First',tempMoney)
    while (tempMoney > 0) {
      if (tempMoney - 500 >= 0) {
        tempMoney -= 500;
        Fivehundred++;
      }else if (tempMoney - 100 >= 0) {
        tempMoney -= 100;
        Onehundred++;
      }else if (tempMoney - 50 >= 0) {
        tempMoney -= 50;
        Fifty++;
      }else if (tempMoney - 10 >= 0) {
        tempMoney -= 10;
        ten++;
      }else if (tempMoney - 5 >= 0) {
        tempMoney -= 5;
        five++;
      }else if (tempMoney - 1 >= 0) {
        tempMoney -= 1;
        one++;
      }
      console.log('tempMoney',tempMoney)
    }
    sum =
      "จะต้องทอน " +
      (Fivehundred > 0 ? "500 = " + Fivehundred + " ใบ / \n" : " ") +
      (Onehundred > 0 ? "100 = " + Onehundred + " ใบ /\n" : " ") +
      (Fifty > 0 ? "50 = " + Fifty + " ใบ / \n" : " ") +
      (ten > 0 ? "10 = " + ten + " เหรียญ / \n" : " ") +
      (five > 0 ? "5 = " + five + " เหรียญ / \n" : " ") +
      (one > 0 ? "1 = " + one + " เหรียญ \n" : " ");
    return sum;
  };
  return (
    <div style={{ padding: 10 }}>
      <div style={{ fontSize: 50 }}>โปรแกรมคิดเงินทอน </div>
      <div style={{ fontSize: 30 }}>ราคาสินค้า </div>
      <input
        className="customSearch"
        value={ProductPrice}
        style={{ paddingLeft: 20 }}
        placeholder={"กรุณากรอกราคาสินค้า"}
        onChange={(e) => {
          let newvalue = e.target.value;
          if (!isNaN(newvalue)) {
            setProductPrice(newvalue === "" ? "" : parseFloat(newvalue));
          }
        }}
      />
      <div style={{ fontSize: 30, marginTop: 40 }}>จำนวนเงินที่รับมา </div>
      <input
        className="customSearch"
        value={money}
        style={{ paddingLeft: 20 }}
        placeholder={"กรุณากรอกจำนวนเงินที่รับมา"}
        onChange={(e) => {
          let newvalue = e.target.value;
          if (!isNaN(newvalue)) {
            setmoney(newvalue === "" ? "" : parseFloat(newvalue));
          }
        }}
      />
      {money != "" && ProductPrice != "" ? (
        <div style={{ fontSize: 30, marginTop: 40 }}>{calculate()} </div>
      ) : null}
    </div>
  );
}
