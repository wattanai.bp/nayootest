import HeaderPage from "@/components/HeaderPage";
import LeftAds from "@/components/LeftAds";
import RightAds from "@/components/RightAds";
import BottomAds from "@/components/BottomAds";
import Head from "next/head";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { useState } from "react";
import CustomNavigatePageButton from "@/components/CustomNavigatePageButton";
export default function No1()  {
    const [TypeMovie, setTypeMovie] = useState([
      {
        name: "หนังล่าสุด",
        img: "/typeLastest.png",
        select: false,
      },
      {
        name: "แอคชั่น",
        img: "/typeAction.png",
        select: false,
      },
      {
        name: "ซีรี่ส์",
        img: "/typeSeries.png",
        select: false,
      },
      {
        name: "การ์ตูน",
        img: "/typeKid.png",
        select: false,
      },
      {
        name: "อนิเมะ",
        img: "/typeAnime.png",
        select: false,
      },
      {
        name: "หนังไทย",
        img: "/typeThai.png",
        select: false,
      },
      {
        name: "หนังฝรั่ง",
        img: "/typeForeign.png",
        select: false,
      },
    ]);
    const [CompanyMovie, setCompanyMovie] = useState([
      "/Tmarvel.png",
      "/TDC.png",
      "/TFox.png",
      "/TParamount.png",
    ]);
    const [Movie, setMovie] = useState([
      {
        img: "/poster (1).png",
        name: "Spider-Man No Way Home (2021) สไปเดอร์แมน โน เวย์ โฮม",
      },
      {
        img: "/poster (2).png",
        name: "Eternals (2021) ฮีโร่พลังเทพเจ้า (ซับไทย)",
      },
      { img: "/poster (3).png", name: "Sompoy (2021) ส้มป่อย" },
      { img: "/poster (4).png", name: "Dont Look Up (2021) (Netflix)" },
      {
        img: "/poster (5).png",
        name: "The Matrix Resurrections (2021) เดอะ เมทริกซ์ เรเซอเร็คชั่นส์",
      },
      {
        img: "/poster (6).png",
        name: "Resident Evil Welcome to Raccoon City (2021) ซับไทย",
      },
      {
        img: "/poster (7).png",
        name: "Venom Let There Be Carnage (2021) เวน่อม 2",
      },
      {
        img: "/poster (8).png",
        name: "Wrath Of Man (2021) คนคลั่งแค้น ปล้นผ่านรก",
      },
      {
        img: "/poster (9).png",
        name: "WandaVision (2021) วันด้าวิสชั่น (Disney Series) 9 ตอน",
      },
      { img: "/poster (10).png", name: "Loki (2021) โลกิ Season 1" },
      {
        img: "/poster (11).png",
        name: "The Suicide Squad (2021) เดอะ ซุยไซด์ สควอด",
      },
      { img: "/poster (12).png", name: "What If (2021)" },
    ]);
    const SlidePage = () => {
      return (
        <div style={{ padding: "55px 250px" }}>
          <Carousel
            autoPlay
            showStatus={false}
            infiniteLoop
            renderArrowPrev={(clickHandler, hasPrev) => {
              return (
                <div
                  style={{
                    position: "absolute",
                    zIndex: 100,
                    top: "40%",
                    left: 0,
                    cursor: "pointer",
                  }}
                  onClick={clickHandler}
                >
                  <img
                    src="/leftArrow.png"
                    style={{ width: 75, objectFit: "contain" }}
                  />
                </div>
              );
            }}
            renderArrowNext={(clickHandler, hasNext) => {
              return (
                <div
                  style={{
                    position: "absolute",
                    zIndex: 100,
                    top: "40%",
                    right: 0,
                    cursor: "pointer",
                  }}
                  onClick={clickHandler}
                >
                  <img
                    src="/rightArrow.png"
                    style={{ width: 75, objectFit: "contain" }}
                  />
                </div>
              );
            }}
            renderIndicator={(onClickHandler, isSelected, index, label) => {
              const defStyle = {
                marginLeft: 20,
                color: "white",
                cursor: "pointer",
                borderRadius: 20,
                padding: "0px 30px",
                fontSize: 8,
              };
              const style = isSelected
                ? { ...defStyle, backgroundColor: "var(--blue2)" }
                : { ...defStyle, backgroundColor: "var(--gray)" };
              return (
                <span
                  style={style}
                  onClick={onClickHandler}
                  onKeyDown={onClickHandler}
                  value={index}
                  key={index}
                  role="button"
                  tabIndex={0}
                  aria-label={`${label} ${index + 1}`}
                >
                  {""}
                </span>
              );
            }}
          >
            <div style={{ paddingBottom: 40 }}>
              <div className="SlideBox displayFlexColCenter">
                <div style={{ fontSize: 71 }}>Slide 1</div>
              </div>
            </div>
            <div style={{ paddingBottom: 40 }}>
              <div className="SlideBox displayFlexColCenter">
                <div style={{ fontSize: 71 }}>Slide 2</div>
              </div>
            </div>
            <div style={{ paddingBottom: 40 }}>
              <div className="SlideBox displayFlexColCenter">
                <div style={{ fontSize: 71 }}>Slide 3</div>
              </div>
            </div>
          </Carousel>
          <img src="/wAds.png" style={{ width: "100%", objectFit: "contain" }} />
        </div>
      );
    };
    const TypeMovieBlog = () => {
      return (
        <div
          className="displayFlexRowBetween"
          style={{ alignItems: "flex-start", padding: "55px 0px 0px 0px" }}
        >
          <img
            src="/hAds.png"
            style={{ width: 234, objectFit: "contain", marginTop: 95 }}
          />
          <div style={{ padding: "0px 16px" }}>
            <div style={{ fontSize: 80, textAlign: "center" }}>หมวดหนัง</div>
            <div className="displayFlexRowBetween">
              <div
                className="movieTypeButton displayFlexColCenter"
                style={{ marginRight: 10, width: "28.5%", height: 430 }}
              >
                <img src={TypeMovie[0].img} />
                <div>{TypeMovie[0].name}</div>
              </div>
              <div
                className="displayFlexRowBetween"
                style={{ flexWrap: "wrap", width: "70%" }}
              >
                {TypeMovie.map((item, i) => {
                  if (i > 0)
                    return (
                      <div
                        key={i}
                        className="movieTypeButton displayFlexColCenter"
                        style={{ marginTop: i > 3 ? 10 : 0 }}
                      >
                        <img src={item.img} />
                        <div>{item.name}</div>
                      </div>
                    );
                })}
              </div>
            </div>
            {CompanyMovieBlog()}
          </div>
          <img
            src="/hAds.png"
            style={{
              width: 234,
              height: "auto",
              objectFit: "contain",
              marginTop: 95,
            }}
          />
        </div>
      );
    };
    const CompanyMovieBlog = () => {
      return (
        <div>
          <div style={{ fontSize: 80, textAlign: "center" }}>ค่ายหนัง</div>
          <Carousel autoPlay showStatus={false} 
            infiniteLoop
            renderArrowPrev={(clickHandler, hasPrev) => {
              return (
                <div
                  style={{
                    position: "absolute",
                    zIndex: 100,
                    top: "30%",
                    left: 0,
                    cursor: "pointer",
                  }}
                  onClick={clickHandler}
                >
                  <img
                    src="/leftArrow.png"
                    style={{ width: 75, objectFit: "contain" }}
                  />
                </div>
              );
            }}
            renderArrowNext={(clickHandler, hasNext) => {
              return (
                <div
                  style={{
                    position: "absolute",
                    zIndex: 100,
                    top: "30%",
                    right: 0,
                    cursor: "pointer",
                  }}
                  onClick={clickHandler}
                >
                  <img
                    src="/rightArrow.png"
                    style={{ width: 75, objectFit: "contain" }}
                  />
                </div>
              );
            }}
            renderIndicator={(onClickHandler, isSelected, index, label) => {
              const defStyle = {
                marginLeft: 20,
                color: "white",
                cursor: "pointer",
                borderRadius: 20,
                padding: "0px 30px",
                fontSize: 8,
              };
              const style = isSelected
                ? { ...defStyle, backgroundColor: "var(--blue2)" }
                : { ...defStyle, backgroundColor: "var(--gray)" };
              return (
                <span
                  style={style}
                  onClick={onClickHandler}
                  onKeyDown={onClickHandler}
                  value={index}
                  key={index}
                  role="button"
                  tabIndex={0}
                  aria-label={`${label} ${index + 1}`}
                >
                  {""}
                </span>
              );
            }}>
            <div className="displayFlexRowBetween" style={{paddingBottom:50}}>
              {CompanyMovie.map((item, i) => {
                return (
                  <div
                    key={i}
                    className="button"
                    style={{
                      flex: 1,
                      marginRight: i != CompanyMovie.length - 1 ? 30 : 0,
                      padding: 0,
                    }}
                  >
                    <img src={item} style={{ width: "100%", height: "100%" }} />
                  </div>
                );
              })}
            </div>
            <div className="displayFlexRowBetween"style={{paddingBottom:50}}>
              {CompanyMovie.map((item, i) => {
                return (
                  <div
                    key={i}
                    className="button"
                    style={{
                      flex: 1,
                      marginRight: i != CompanyMovie.length - 1 ? 30 : 0,
                      padding: 0,
                    }}
                  >
                    <img src={item} style={{ width: "100%", height: "100%" }} />
                  </div>
                );
              })}
            </div>
            <div className="displayFlexRowBetween" style={{paddingBottom:50}}>
              {CompanyMovie.map((item, i) => {
                return (
                  <div
                    key={i}
                    className="button"
                    style={{
                      flex: 1,
                      marginRight: i != CompanyMovie.length - 1 ? 30 : 0,
                      padding: 0,
                    }}
                  >
                    <img src={item} style={{ width: "100%", height: "100%" }} />
                  </div>
                );
              })}
            </div>
          </Carousel>
        </div>
      );
    };
    const MovieBlog = () => {
      return (
        <div className="displayFlexRow" style={{ alignItems: "flex-start" }}>
          <div style={{ width: 234, marginTop: 95 }}>
            <div style={{ fontSize: 42, padding: 10 }}>ปีที่ฉาย</div>
            <div className="displayFlexRow" style={{ flexWrap: "wrap" }}>
              {[
                2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023,
                2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023, 2023,
              ].map((item, i) => {
                return (
                  <div key={i} className="leftbutton">
                    {item}
                  </div>
                );
              })}
            </div>
            <div style={{ fontSize: 42, padding: 10 }}>หมวดหมู่</div>
            <div className="displayFlexRow" style={{ flexWrap: "wrap" }}>
              {[
                "• หมวดหมู่",
                "• หนังซูม",
                "• หนังฝรั่ง",
                "• หนังไทย",
                "• ซีรี่ส์",
                "• การ์ตูน",
                "• อนิเมะ",
              ].map((item, i) => {
                return (
                  <div
                    key={i}
                    className="leftbutton"
                    style={{ width: "100%", textAlign: "left", paddingLeft: 10 }}
                  >
                    {item}
                  </div>
                );
              })}
            </div>
            <div style={{ fontSize: 42, padding: 10 }}>ประเภท</div>
            <div className="displayFlexRow" style={{ flexWrap: "wrap" }}>
              {[
                "• Biography ชีวประวัติ",
                "• Adventure ผจญภัย",
                "• Action แอคชั่น",
                "• Comedy ตลก",
                "• History ประวัติศาสตร์",
                "• Horror สยองขวัญ",
                "• Western ตะวันตก",
                "• War สงคราม",
                "• Thriller ระทึกขวัญ",
                "• Sci-fi วิทยาศาสตร์",
                "• Romance โรแมนติก",
                "• Mystery ลึกลับ",
                "• Musical ดนตรี",
                "• Fantasy แฟนตาซี",
                "• Family ครอบครัว",
                "• Drama ชีวิต",
                "• Documentary สารคดี",
                "• Crime อาชญากรรม",
              ].map((item, i) => {
                return (
                  <div
                    key={i}
                    className="leftbutton"
                    style={{
                      width: "100%",
                      textAlign: "left",
                      paddingLeft: 10,
                      paddingRight: 5,
                    }}
                  >
                    {item}
                  </div>
                );
              })}
            </div>
          </div>
          <div style={{ padding: "0px 16px", flex: 1 }}>
            <div
              className="displayFlexRow"
              style={{
                backgroundColor: "var(--gray)",
                padding: "5px 0px",
                marginBottom: 20,
              }}
            >
              {[
                "อัพเดตล่าสุด",
                "หนังเรียงตามปี",
                "TOP IMDM",
                "คนชอบมากที่สุด",
              ].map((item, i) => {
                return (
                  <div className="displayFlexRowAlignCenter">
                    <div
                      key={i}
                      className="button"
                      style={{ fontSize: 50, borderRadius: 0 }}
                    >
                      {item}
                    </div>
                    <div style={{ height: 40, borderRight: "6px solid black" }} />
                  </div>
                );
              })}
            </div>
            <div className="displayFlexColAlignCenter">
              <div
                className="displayFlexRowBetween"
                style={{ flexWrap: "wrap", width: "100%" }}
              >
                {Movie.map((item, i) => {
                  return (
                    <div
                      key={i}
                      className="movieButton displayFlexColCenter"
                      style={{ marginTop: i > 3 ? 20 : 0 }}
                    >
                      <div
                        className="displayFlexRowAlignCenter"
                        style={{
                          position: "absolute",
                          left: 5,
                          top: 5,
                          zIndex: 3,
                          backgroundColor: "var(--red2)",
                          padding: 5,
                          borderRadius: 30,
                        }}
                      >
                        <img
                          src={"/heart_outline.png"}
                          style={{
                            width: 25,
                            height: 25,
                            objectFit: "contain",
                            marginRight: 5,
                          }}
                        />
                        <div style={{ fontSize: 25, color: "white" }}>9.8</div>
                      </div>
  
                      <img
                        src={"/top.png"}
                        style={{
                          width: 70,
                          height: 70,
                          objectFit: "contain",
                          position: "absolute",
                          right: 0,
                          top: 0,
                          zIndex: 3,
                        }}
                      />
                      <img id="bg" src={item.img} />
                      <div id="box">
                        <div id="text">{item.name}</div>
                      </div>
                    </div>
                  );
                })}
              </div>
              <CustomNavigatePageButton AllPage={5} />
            </div>
          </div>
          <img
            src="/hAds.png"
            style={{
              width: 234,
              height: "auto",
              objectFit: "contain",
            }}
          />
        </div>
      );
    };
    return (
      <>
        <Head>
          <title>#1</title>
          <meta name="description" content="Generated by create next app" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <div>
          <HeaderPage />
          {SlidePage()}
          {TypeMovieBlog()}
          {MovieBlog()}
          <LeftAds />
          <RightAds />
          <BottomAds />
        </div>
      </>
    );
  }
  